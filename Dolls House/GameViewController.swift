//
//  GameViewController.swift
//  Dolls House
//
//  Created by Matt Gannon on 1/16/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            
            
            if #available(iOS 11.0, *) {
//                preferredScreenEdgesDeferringSystemGestures()
                setNeedsUpdateOfScreenEdgesDeferringSystemGestures()
            } else {
                // Fallback on earlier versions
            }
            
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "GameScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .resizeFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            //Swap these to see dev things
            view.showsPhysics = false;
            view.showsFPS = false;
            view.showsNodeCount = false;
        }
    }

    override func prefersHomeIndicatorAutoHidden() -> Bool {
        return true
    }
    
    override func preferredScreenEdgesDeferringSystemGestures() -> UIRectEdge {
        return .all
    }

    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
