//
//  Polaroid.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/1/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class Polaroid: HMSprite {
    
    var headsShown = false;
    var viewSize :CGSize?
//    var viewWidth = CGFloat(0.0);
//    var viewHeight = CGFloat(0.0);
    
    var headArray :[Head] = [Head]()    //array for the head sprites that appear when the polaroid is clicked

    init(dolls:[Doll], size: CGSize){
        let texture = SKTexture(imageNamed: "polaroid")
        viewSize = size
        
        super.init(texture: texture, color: UIColor.clear, size: texture.size())

        self.size = resize(sprite: self, scale: 0.185)
        
        self.zPosition = 50;
    }
    
    // animation for showing character selection
    func showChars() {
        print("showing chars")
        let dur = Double(0.18)
        for head in headArray {
            head.zPosition = 51;
            head.position = CGPoint(x: self.position.x, y: self.position.y);
        }
        
        var c = 1.0
        for head in headArray {
            let cFloat = CGFloat(c)
            var move = SKAction()
            let width = Float((viewSize?.width)!)
            let height = Float((viewSize?.height)!)
            if (((height/width) < 0.751) && (height/width) > 0.74) {
                move = SKAction.moveTo(x: self.position.x + (viewSize?.width)!*cFloat/10, duration: dur*c)
            } else {
                move = SKAction.moveTo(x: self.position.x + (viewSize?.width)!*cFloat/12, duration: dur*c)
            }
            let newHeight = self.size.height
//            print("new size is: ",newSize)
            let size = SKAction.resize(toWidth: head.ratio!*newHeight, height: newHeight, duration: dur*c)
            let scale = SKAction.scale(to: 0.5, duration: dur*c)
            let scaleGroup = SKAction.group([move, scale, size])
            
//            head.run(move)
            head.run(scaleGroup)
            
            c+=1;
        }
        
        headsShown = true;
    }
    
    func hideChars() {
        print("hiding chars")
        let dur = Double(0.18)
        //set actions for animation
        var c = 1.0
        for head in headArray {
            let move = SKAction.moveTo(x: self.position.x, duration: dur*c)
            let scale = SKAction.scale(to: 0, duration: dur*c)
            let scaleGroup = SKAction.group([move, scale])
            
//            head.run(move)
            head.run(scaleGroup)
            
            c+=1;
        }
        headsShown = false;
    }
    
    override func moveChar(to location: CGPoint?) {
        //polaroid can't move
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func resize(sprite: HMSprite, scale: CGFloat) -> CGSize {
        let widthScale = sprite.size.width/sprite.size.height
        let height = scale*(viewSize?.height)!
        let width = widthScale*height
        let size =  CGSize(width: width, height: height)
        
        print(sprite.size.width, sprite.size.height, widthScale, height, width, size)
        return size
    }
    
    
}


