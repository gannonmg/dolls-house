//
//  CollisionObjects.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/8/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class CollisionObject: SKSpriteNode {
    
    var state = SpriteOptions.idle;
    var collider:Collider?
        
    init(myCollider: Collider, size viewSize: CGSize) {
        collider = myCollider
        let viewWidth = viewSize.width
        let viewHeight = viewSize.height
        let size = CGSize(width: CGFloat((collider?.width)!)*viewWidth, height: CGFloat((collider?.height)!)*viewHeight)
        
        super.init(texture: nil, color: UIColor.clear, size: size)
        
        state = SpriteOptions.stringToSprite(string: (collider?.givesState)!)!
        
        let colliderShape = SKShapeNode(rectOf: size)
        self.addChild(colliderShape)
        
//        collider.strokeColor = .red //for visual testing
//        collider.zPosition = 10  //for visual testing

    }
    
    var empty = true;
    
    func addCollisionChild(sprite: Character) {
        let canInteract = sprite.doll?.states[SpriteOptions.stateToString(state: state)!] != nil;
        if canInteract {
            if empty {
                sprite.name = "collisionChild"
                
                empty = false
                
                sprite.position = CGPoint.zero
        
                sprite.physicsBody?.isDynamic = false
                sprite.physicsBody!.affectedByGravity = false
                
                sprite.stance(to: state)
                
                self.removeAllChildren()
                sprite.removeFromParent()
                self.addChild(sprite)
                
                if let sound = collider?.sound {
                    let soundAction = SKAction.playSoundFileNamed(sound, waitForCompletion: true)
                    self.run(soundAction)
                }
                
                sprite.zPosition = 1
                
                sprite.physicsBody?.affectedByGravity = false
                
            }
        }
    }

    
    init(texture: SKTexture!) {
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
    }
    
    override init(texture: SKTexture!, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func givesState() -> SpriteOptions {
        return state;
    }
    
    
}
