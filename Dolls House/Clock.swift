//
//  Clock.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/1/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class Clock: HMSprite {
    
    
    override init() {
        let texture = SKTexture(imageNamed: "clock")
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
    }
    
    override func moveChar(to location: CGPoint?) {
        //clock can't move
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
