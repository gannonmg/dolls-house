//
//  TireSwing.swift
//  Dolls House
//
//  Created by Matt Gannon on 1/31/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class TireSwing: HMSprite {
    
    //Swing initializer
    var swingArray :[SKSpriteNode] = [SKSpriteNode]()
    
    //Swing stuff
    var tireSize = CGSize(width: 75, height: 75);
    var ropeSize = CGSize(width: 10, height: 30);
    
    //joints & pieces
    let rope1 = SKSpriteNode(imageNamed: "rope")
    let rope2 = SKSpriteNode(imageNamed: "rope")
    let rope3 = SKSpriteNode(imageNamed: "rope")
    let tire = SKSpriteNode(imageNamed: "tire")
    
    var jointArray :[SKPhysicsJointPin] = [SKPhysicsJointPin]();
    
    init(position pos: CGPoint, size: CGSize) {
        super.init(texture: nil, color: UIColor.clear, size: CGSize.zero)
        
        let viewSize = size
        
        tireSize = CGSize(width: viewSize.height*0.23, height: viewSize.height*0.23);
        ropeSize = CGSize(width: viewSize.width*0.0123, height: viewSize.height*0.08);
        print("rope size width is", ropeSize.width)
        print("rope size height is", ropeSize.height)
        
        rope1.size = ropeSize
        rope2.size = ropeSize
        rope3.size = ropeSize
        tire.size = tireSize
        
        swingArray.append(rope1)
        swingArray.append(rope2)
        swingArray.append(rope3)
        swingArray.append(tire)
        
        makeSwing();
        
        self.position = pos;
        self.physicsBody = SKPhysicsBody()
        self.physicsBody?.isDynamic = false
        
        rope1.position = CGPoint(x: 0, y: ropeSize.height/2)
        rope1.physicsBody = SKPhysicsBody(rectangleOf: rope1.size)
        rope2.physicsBody = SKPhysicsBody(rectangleOf: rope2.size)
        rope3.physicsBody = SKPhysicsBody(rectangleOf: rope3.size)
        
        for piece in swingArray {
            piece.name = "swing"
            piece.physicsBody?.categoryBitMask = PhysicsCategories.Swing;
            piece.physicsBody?.contactTestBitMask = PhysicsCategories.Character
            piece.physicsBody?.collisionBitMask = PhysicsCategories.House;
            piece.physicsBody?.density = 5.0
        }
        
    }
    
    override func update(dt: TimeInterval) {
        if let location = desiredLocation {
            let dt = CGFloat(dt*20)
            let dx = diffCheck(diff: CGFloat(location.x-self.position.x));
            let dy = diffCheck(diff: CGFloat(location.y-self.position.y));
            var vel = CGVector(dx: dx/dt, dy: dy/dt)
            vel = velocityCheck(vel: vel);
            tire.physicsBody?.velocity = vel
        }
    }
    
    func diffCheck(diff: CGFloat) -> CGFloat {
        var newDiff = diff;
        let maxDiff = CGFloat(210.0)
        if (diff > maxDiff) {
            newDiff = maxDiff
        } else if (diff < -maxDiff) {
            newDiff = -maxDiff
        }
        return newDiff
    }
    
    func velocityCheck(vel: CGVector) -> CGVector {
        var velocity = vel;
        let MAX_VELOCITY = CGFloat(700.0)
        if (velocity.dx > MAX_VELOCITY) {
            velocity.dx = MAX_VELOCITY
        } else if (velocity.dx < -MAX_VELOCITY) {
            velocity.dx = -MAX_VELOCITY
        }
        if (velocity.dy > MAX_VELOCITY) {
            velocity.dy = MAX_VELOCITY
        } else if (velocity.dy < -MAX_VELOCITY) {
            velocity.dy = -MAX_VELOCITY
        }
        return velocity;
    }


    
    func addToScene(scene: HouseScene) {
        scene.addChild(self)
        self.addChild(rope1)

        let pinJoint1 = SKPhysicsJointPin.joint(withBodyA: self.physicsBody!,
                                               bodyB: rope1.physicsBody!,
                                               anchor: self.position)
        scene.physicsWorld.add(pinJoint1)
        rope1.addChild(rope2)
        let pinJoint2 = SKPhysicsJointPin.joint(withBodyA: rope1.physicsBody!,
                                               bodyB: rope2.physicsBody!,
                                               anchor: CGPoint(x: self.position.x+rope1.position.x, y: self.position.y+rope1.position.y+ropeSize.height/2))
        scene.physicsWorld.add(pinJoint2)

        rope2.addChild(rope3)
        let pinJoint3 = SKPhysicsJointPin.joint(withBodyA: rope2.physicsBody!,
                                                bodyB: rope3.physicsBody!,
                                                anchor: CGPoint(x: self.position.x+rope2.position.x, y: self.position.y+rope2.position.y))
        scene.physicsWorld.add(pinJoint3)

        rope3.addChild(tire)
        let pinJoint4 = SKPhysicsJointPin.joint(withBodyA: rope3.physicsBody!,
                                                bodyB: tire.physicsBody!,
                                                anchor: CGPoint(x: self.position.x+rope3.position.x, y: self.position.y+rope3.position.y+tireSize.height/2))
        scene.physicsWorld.add(pinJoint4)
        
//        scene.addChild(self)
//        self.addChild(rope1)
//
//        let pinJoint1 = SKPhysicsJointPin.joint(withBodyA: self.physicsBody!,
//                                                bodyB: rope1.physicsBody!,
//                                                anchor: CGPoint(x: rope1.position.x, y: rope1.position.y+rope1.size.height/2))
//        scene.physicsWorld.add(pinJoint1)
//        rope1.addChild(rope2)
//        let pinJoint2 = SKPhysicsJointPin.joint(withBodyA: rope1.physicsBody!,
//                                                bodyB: rope2.physicsBody!,
//                                                anchor: CGPoint(x: rope2.position.x, y: rope2.position.y+rope2.size.height/2))
//        scene.physicsWorld.add(pinJoint2)
//
//        rope2.addChild(rope3)
//        let pinJoint3 = SKPhysicsJointPin.joint(withBodyA: rope2.physicsBody!,
//                                                bodyB: rope3.physicsBody!,
//                                                anchor: CGPoint(x: rope3.position.x, y: rope3.position.y+rope3.size.height/2))
//        scene.physicsWorld.add(pinJoint3)
//
//        rope3.addChild(tire)
//        let pinJoint4 = SKPhysicsJointPin.joint(withBodyA: rope3.physicsBody!,
//                                                bodyB: tire.physicsBody!,
//                                                anchor: CGPoint(x: tire.position.x, y: tire.position.y+tireSize.height/2))
//        scene.physicsWorld.add(pinJoint4)

        
        jointArray.append(pinJoint1)
        jointArray.append(pinJoint2)
        jointArray.append(pinJoint3)
        jointArray.append(pinJoint4)
        
        for joint in jointArray {
            joint.shouldEnableLimits = true
            joint.upperAngleLimit = 3.5
            joint.lowerAngleLimit = -3.5
            joint.frictionTorque = 0.7
        }
        
        pinJoint4.lowerAngleLimit = -3
        pinJoint4.upperAngleLimit = 3
        
        
        tire.position = CGPoint(x: tire.position.x-30, y: tire.position.y-50)
    }
    
    
    func jointForceCheck(velocity: CGVector) {
        let MAX_VELOCITY = CGFloat(13)
        if (velocity.dx > MAX_VELOCITY) {
            self.desiredLocation = nil
        } else if (velocity.dx < -MAX_VELOCITY) {
            self.desiredLocation = nil
        }
        if (velocity.dy > MAX_VELOCITY) {
            self.desiredLocation = nil
        } else if (velocity.dy < -MAX_VELOCITY) {
            self.desiredLocation = nil
        }
    }

    var first = true
    
    func redundantRestrain() -> Bool {
        var shouldRestrain = false
        let maxSpringDistance: CGFloat = 2.5*ropeSize.height
        
        for joint in jointArray {
            if let bodyBPosition = joint.bodyB.node?.position {
                let distance = hypot(bodyBPosition.x, bodyBPosition.y)
//                if distance > maxSpringDistance*1.5 {
//                    joint.bodyB.node?.position = CGPoint.zero;
//                }
                if distance > maxSpringDistance {
                    desiredLocation = nil
                    shouldRestrain = true
                }
                let tireDistance = hypot(self.tire.position.x, self.tire.position.y)
                let maxDistance = ((3*ropeSize.height) + (tireSize.height/2))/2
                if tireDistance > maxDistance {
                    joint.bodyB.node?.position = CGPoint.zero
                    desiredLocation = nil
                    shouldRestrain = true
                }

            }
        }
        for joint in jointArray {
            jointForceCheck(velocity: joint.reactionForce)
        }
        
        return shouldRestrain
    }
    
    func restrain(touch: UITouch, scene: SKScene, viewSize: CGSize) -> Bool {
        let location = touch.location(in: scene)
        let distance = hypot(self.position.x-location.x, self.position.y-location.y)
        if distance > 0.65*viewSize.height {
            self.moveChar(to: nil)
            self.rope1.physicsBody?.velocity = CGVector.zero
            self.rope2.physicsBody?.velocity = CGVector.zero
            self.rope3.physicsBody?.velocity = CGVector.zero
            self.tire.physicsBody?.velocity = CGVector.zero
            return true
        } else {
            self.moveChar(to: location)
            return false
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func getBody() -> SKPhysicsBody {
        let body = SKPhysicsBody(bodies: [rope1.physicsBody!, rope2.physicsBody!, rope3.physicsBody!, tire.physicsBody!])
        return body;
    }
    
    func givesState() -> SpriteOptions {
        return SpriteOptions.swinging;
    }
    
    func makeSwing() {
        for piece in swingArray {
            if piece == tire {
                piece.physicsBody = SKPhysicsBody(circleOfRadius: piece.size.width/2)
            } else {
                piece.physicsBody = SKPhysicsBody(rectangleOf: piece.size)
            }
            piece.zPosition = 1;
            piece.physicsBody?.usesPreciseCollisionDetection = true
        }
    }
    
    var emptySwing = true;
    
    func addTireChild(sprite: Character) {
        let canSwing = sprite.doll?.states["swinging"] != nil;
        if canSwing {
            if emptySwing {
                sprite.name = "tireChild"
                emptySwing = false
                sprite.stance(to: SpriteOptions.swinging)
                sprite.position = tire.position
                tire.removeAllChildren()
                sprite.removeFromParent()
                tire.addChild(sprite)
                sprite.zPosition = (tire.zPosition+1);
            }
        }
    }
    
}
