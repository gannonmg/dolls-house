//
//  SpriteOptions.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/6/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation

struct SpriteOptions: OptionSet {
    let rawValue: Int
    
    static let basementLeft    = SpriteOptions(rawValue: 1 << 0)
    static let basementRight  = SpriteOptions(rawValue: 1 << 1)
    static let livingRoom   = SpriteOptions(rawValue: 1 << 2)
    static let diningRoom   = SpriteOptions(rawValue: 1 << 3)
    static let kitchen    = SpriteOptions(rawValue: 1 << 4)
    static let backyard  = SpriteOptions(rawValue: 1 << 5)
    static let parentsRoom   = SpriteOptions(rawValue: 1 << 6)
    static let kidsRoom   = SpriteOptions(rawValue: 1 << 7)
    static let bathroom  = SpriteOptions(rawValue: 1 << 8)
    static let attic   = SpriteOptions(rawValue: 1 << 9)
    
    static let idle   = SpriteOptions(rawValue: 1 << 10)
    static let sitting   = SpriteOptions(rawValue: 1 << 11)
    static let laying  = SpriteOptions(rawValue: 1 << 12)
    static let pickedUp   = SpriteOptions(rawValue: 1 << 13)
    static let peeing   = SpriteOptions(rawValue: 1 << 14)
    static let bathing   = SpriteOptions(rawValue: 1 << 15)
    static let swinging   = SpriteOptions(rawValue: 1 << 16)
    static let begging  = SpriteOptions(rawValue: 1 << 17)
    static let pullup  = SpriteOptions(rawValue: 1 << 18)
    static let eating   = SpriteOptions(rawValue: 1 << 19)
    
    static let rooms: SpriteOptions = [.basementLeft, .basementRight, .livingRoom, .diningRoom, .kitchen, .backyard, .parentsRoom, .kidsRoom, .bathroom, .attic]
    static let stances: SpriteOptions = [.idle, .sitting, .laying, .pickedUp, .peeing, .bathing, .swinging, .begging, .pullup, .eating]

    
    static func stringToSprite(string: String) -> SpriteOptions? {
        if string == "basementLeft" { return SpriteOptions.basementLeft }
        if string == "basementRight" { return SpriteOptions.basementRight }
        if string == "livingRoom" { return SpriteOptions.livingRoom }
        if string == "diningRoom" { return SpriteOptions.diningRoom }
        if string == "kitchen" { return SpriteOptions.kitchen }
        if string == "backyard" { return SpriteOptions.backyard }
        if string == "parentsRoom" { return SpriteOptions.parentsRoom }
        if string == "kidsRoom" { return SpriteOptions.kidsRoom }
        if string == "bathroom" { return SpriteOptions.bathroom }
        if string == "attic" { return SpriteOptions.attic }
        
        if string == "idle" { return SpriteOptions.idle }
        if string == "sitting" { return SpriteOptions.sitting }
        if string == "laying" { return SpriteOptions.laying }
        if string == "pickedUp" { return SpriteOptions.pickedUp }
        if string == "peeing" { return SpriteOptions.peeing }
        if string == "bathing" { return SpriteOptions.bathing }
        if string == "swinging" { return SpriteOptions.swinging }
        if string == "begging" { return SpriteOptions.begging }
        if string == "pullup" { return SpriteOptions.pullup }
        if string == "eating" { return SpriteOptions.eating }
        
        return nil
    }
    
    
    static func stateToString(state: SpriteOptions) -> String? {
        if state == SpriteOptions.basementLeft { return "basementLeft" }
        if state == SpriteOptions.basementRight { return "basementRight" }
        if state == SpriteOptions.livingRoom { return "livingRoom" }
        if state == SpriteOptions.diningRoom { return "diningRoom" }
        if state == SpriteOptions.kitchen { return "kitchen" }
        if state == SpriteOptions.backyard { return "backyard" }
        if state == SpriteOptions.parentsRoom { return "parentsRoom" }
        if state == SpriteOptions.kidsRoom { return "kidsRoom" }
        if state == SpriteOptions.bathroom { return "bathroom" }
        if state == SpriteOptions.attic { return "attic" }
        
        if state == SpriteOptions.idle { return "idle" }
        if state == SpriteOptions.sitting { return "sitting" }
        if state == SpriteOptions.laying { return "laying" }
        if state == SpriteOptions.pickedUp { return "pickedUp" }
        if state == SpriteOptions.peeing { return "peeing" }
        if state == SpriteOptions.bathing { return "bathing" }
        if state == SpriteOptions.swinging { return "swinging"}
        if state == SpriteOptions.begging { return "begging"}
        if state == SpriteOptions.pullup { return "pullup"}
        if state == SpriteOptions.eating { return "eating"}
        
        return nil
    }


}

