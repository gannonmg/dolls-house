//
//  GameScene.swift
//  Dolls House
//
//  Created by Matt Gannon on 1/16/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    override func didMove(to view: SKView) {
        
        //Swap these to see dev things
        view.showsPhysics = false;
        view.showsFPS = false;
        view.showsNodeCount = false;
        
        print("view size for this iphone is: ", view.frame.width, view.frame.height)
        
        if view.frame.width == 812.0 {
            let welcome = SKSpriteNode(imageNamed: "startScreenX");
            welcome.size = view.frame.size
            self.addChild(welcome)
        } else {
            let welcome = SKSpriteNode(imageNamed: "startScreen");
            welcome.size = view.frame.size
            self.addChild(welcome)
        }
        
        let wait = SKAction.wait(forDuration: 0.6)
        let block = SKAction.run {
            self.changeScenes()
        }
        let sequence = SKAction.sequence([wait, block])
        self.run(sequence)
        
    }
    
    
    func changeScenes() {
        let houseScene = HouseScene(fileNamed: "HouseScene");
        houseScene?.scaleMode = .resizeFill
        self.view?.presentScene(houseScene!, transition: SKTransition.fade(withDuration: 0.1));

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        changeScenes()
    }
    
    
    
}
