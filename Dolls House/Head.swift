//
//  Head.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/1/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class Head: HMSprite {
    
    var mySprite :Character?;
    
    var ratio :CGFloat?
    
    init(imageNamed image: String, sprite: Character) {
        let texture = SKTexture(imageNamed: image)
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
        
        mySprite = sprite;
        
        ratio = self.size.width/self.size.height
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

