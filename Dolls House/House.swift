//
//  House.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/1/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class House: SKSpriteNode {

    var rainbow = false
    
    var floor3Array :[SKSpriteNode] = [SKSpriteNode]();
    let roofLeft = SKSpriteNode(imageNamed: "rooflineLeft");
    let attic = SKSpriteNode(imageNamed: "attic");
    let roofRight = SKSpriteNode(imageNamed: "rooflineRight");
    let sky = SKSpriteNode(imageNamed: "backyard3");
    
    var floor2Array :[SKSpriteNode] = [SKSpriteNode]();
    let parentsRoom = SKSpriteNode(imageNamed: "parentsRoom");
    let kidsRoom = SKSpriteNode(imageNamed: "kidsRoom");
    let bathroom = SKSpriteNode(imageNamed: "bathroom");
    let backyardUpper = SKSpriteNode(imageNamed: "backyard2");
    
    var floor1Array :[SKSpriteNode] = [SKSpriteNode]();
    let livingRoom = SKSpriteNode(imageNamed: "livingRoom");
    let diningRoom = SKSpriteNode(imageNamed: "diningRoom");
    let kitchen = SKSpriteNode(imageNamed: "kitchen");
    let backyard = SKSpriteNode(imageNamed: "backyard1");
    
    var floor0Array :[SKSpriteNode] = [SKSpriteNode]();
    let dirtLeft = SKSpriteNode(imageNamed: "soilLeft");
    let basementLeft = SKSpriteNode(imageNamed: "basementLeft");
    let basementRight = SKSpriteNode(imageNamed: "basementRight");
    let dirtRight = SKSpriteNode(imageNamed: "soilRight");
    
    var allFloorsArray :[Array] = [Array<SKSpriteNode>]()
    
    //array to hold center points of each room (to center camera)
    var centerArray :[CGPoint] = [CGPoint]()
    
    
    var viewSize = CGSize.zero
    var viewWidth = CGFloat(0.0)
    var viewHeight = CGFloat(0.0)
    
    init() {
        super.init(texture: nil, color: UIColor.clear, size: CGSize.zero)
        
//        roofLeft.name = "nil"
        attic.name = "attic"
//        roofRight.name = "roof"
        parentsRoom.name = "parentsRoom"
        kidsRoom.name = "kidsRoom"
        bathroom.name = "bathroom"
        livingRoom.name = "livingRoom"
        diningRoom.name = "diningRoom"
        kitchen.name = "kitchen"
        backyard.name = "backyard"
        basementLeft.name = "basementLeft"
        basementRight.name = "basementRight"
        dirtLeft.name = nil
        
    }
    
    func setViewSize(size: CGSize) {
        viewSize = size;
        viewWidth = viewSize.width;
        viewHeight = viewSize.height;
    }
    
    func drawHouse() {
        //append rooms to their individual floors arrays
        floor3Array.append(roofLeft)
        floor3Array.append(attic)
        floor3Array.append(roofRight)
        floor3Array.append(sky)
        floor2Array.append(parentsRoom)
        floor2Array.append(kidsRoom)
        floor2Array.append(bathroom)
        floor2Array.append(backyardUpper)
        floor1Array.append(livingRoom)
        floor1Array.append(diningRoom)
        floor1Array.append(kitchen)
        floor1Array.append(backyard)
        floor0Array.append(dirtLeft)
        floor0Array.append(basementLeft)
        floor0Array.append(basementRight)
        floor0Array.append(dirtRight)
        
        //append floor arrays to array holding all floor info
        allFloorsArray.append(floor0Array)
        allFloorsArray.append(floor1Array)
        allFloorsArray.append(floor2Array)
        allFloorsArray.append(floor3Array)
        
        var roomX = CGFloat(0.0);
        var roomY = CGFloat(0.0);
        
        for floor in allFloorsArray {
            roomX = 0.0;
            for room in floor {
//                print("room name: ", room.name)
                room.position = CGPoint(x: roomX, y: roomY);
                centerArray.append(room.position);
//                room.size = CGSize(width: , height: viewHeight);
                room.size = viewSize;
                
//                while room.size.width > viewWidth  {
//                    room.xScale -= 0.01
//                    room.yScale -= 0.01
//                }
                
                room.zPosition = 0;
                self.addChild(room);
                roomX += viewWidth;
            }
            roomY += viewHeight;
        }
    }
    
    var borderArray :[SKShapeNode] = [SKShapeNode]()
    
    //Make physicals borders
    func drawBorders() {
        var borderArray :[SKShapeNode] = [SKShapeNode]()
        
        let groundY = CGFloat(-viewHeight*4/9)
        let wallX = CGFloat(-viewWidth/2)
        
        for c in 0...4 {
            var groundPoints = [CGPoint(x: -viewWidth*4, y: groundY+(viewHeight*CGFloat(c))),
                                CGPoint(x: viewWidth*4, y: groundY+viewHeight*CGFloat(c))]
            let ground = SKShapeNode(splinePoints: &groundPoints, count: groundPoints.count)
            var wallPoints = [CGPoint(x: wallX+(viewWidth*CGFloat(c)), y: viewHeight*4),
                              CGPoint(x: wallX+(viewWidth*CGFloat(c)), y: -viewHeight*4)]
            let wall = SKShapeNode(splinePoints: &wallPoints, count: wallPoints.count)
            
            ground.name = "house"
            wall.name = "house"
            
            borderArray.append(ground)
            borderArray.append(wall)
        }
        
        for border in borderArray {
            border.lineWidth = 5
            border.strokeColor = .clear
//            border.strokeColor = .red
//            border.zPosition = 10  //for visual testing
            border.physicsBody = SKPhysicsBody(edgeChainFrom: border.path!)
            border.physicsBody?.isDynamic = false
            border.physicsBody?.restitution = 0.0
            self.addChild(border)
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


}
