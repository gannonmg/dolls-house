//
//  Character.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/1/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class Character: HMSprite {
    
    var doll :Doll?
    var viewSize :CGSize?
//    let scaler = CGFloat(0.45)
    
    override var currentState :SpriteOptions {
        didSet {
            //Do state changes
            var stanceCopy = currentState;
            stanceCopy.remove(SpriteOptions.rooms)
            let stanceName = SpriteOptions.stateToString(state: stanceCopy)
            
            var roomCopy = currentState;
            roomCopy.remove(SpriteOptions.stances)
            let roomName = SpriteOptions.stateToString(state: roomCopy)
//            print(roomName, stanceName)
            if self.parent is SKScene {
                onStateChanged(stance: stanceName, location: roomName)
            }
        }
    }

    func onStateChanged(stance: String?, location: String?) {
        guard let doll = doll else { return }
        guard let stance = stance else { return }
        guard let location = location else { return }
        
        self.removeAllActions()
        
        if let state = doll.states[stance] {
            var texture :SKTexture?
            
            if let spriteLocation = state.locations?[location] {
                if let animation = doll.animations?[spriteLocation] {
                    animate(animation, stance)
                    return
                } else {
                    texture = SKTexture(imageNamed: spriteLocation)
                    self.texture = texture
                    self.size = (self.texture?.size())!
//                    self.size = CGSize(width: self.size.width*scaler, height: self.size.height*scaler)
                    let heightScale = self.size.height/736
                    let widthScale = self.size.width/self.size.height
                    let height = heightScale*(viewSize?.height)!
                    let width = widthScale*height
                    self.size = CGSize(width: width*1.75, height: height*1.75)
                }
            } else {
                texture = SKTexture(imageNamed: state.texture)
                self.texture = texture
                self.size = (self.texture?.size())!
//                self.size = CGSize(width: self.size.width*scaler, height: self.size.height*scaler)
                let heightScale = self.size.height/736
                let widthScale = self.size.width/self.size.height
                let height = heightScale*(viewSize?.height)!
                let width = widthScale*height
                self.size = CGSize(width: width*1.75, height: height*1.75)
            }
            
            if let anim = state.animation, let animation = doll.animations?[anim] {
                animate(animation, stance)
                return
            }
            
            if let sound = state.sound {
                let soundAction = SKAction.playSoundFileNamed(sound, waitForCompletion: true)
                self.run(soundAction)
            }
            
//            self.removeAllActions()
            self.setPhysics()
        }
    }

    func animate(_ animation: Doll.Animation, _ stance: String) {
        let frames = animation.frames
        
        var textureArray :[SKTexture] = [SKTexture]()
        
        for frame in frames {
            let texture = SKTexture(imageNamed: frame)
            textureArray.append(texture)
        }
        
        if !textureArray.isEmpty {
            let texture = textureArray[0]
            self.size = texture.size()
            let heightScale = self.size.height/736
            let widthScale = self.size.width/self.size.height
            let height = heightScale*(viewSize?.height)!
            let width = widthScale*height
            self.size = CGSize(width: width*1.75, height: height*1.75)
            
            self.setPhysics()
        }
        
        let myTimePerFrame = 0.55
        let animate = SKAction.animate(with: textureArray, timePerFrame: myTimePerFrame)
        if stance == "idle" {
            if animation.repeats {
                let repeatFor = Int(arc4random_uniform(5)) + 2
                let animateFor = SKAction.repeat(animate, count: repeatFor)
                let velocity = CGFloat(animation.speed) * CGFloat(animation.startDirection)
                let moveTime = Double(repeatFor * frames.count) * myTimePerFrame
                let moveFor = SKAction.move(by: CGVector(dx: velocity, dy: 0), duration: TimeInterval(moveTime))
                let group = SKAction.group([animateFor, moveFor])
                let waitTime = Double(arc4random_uniform(4)) + 2
                let wait = SKAction.wait(forDuration: waitTime)
                let sequence = SKAction.sequence([group, wait])
                self.run(SKAction.repeatForever(sequence))
            } else {
                self.run(animate)
            }
        } else {
            if animation.repeats {
                self.run(SKAction.repeatForever(animate))
            } else {
                self.run(animate)
            }
        }
    }
    
    
    func setPhysics() {
        if let oldVel = self.physicsBody?.velocity {
            if self.currentState.contains(SpriteOptions.swinging) {
                self.physicsBody = SKPhysicsBody(texture: self.texture!, alphaThreshold:0, size: CGSize(width: self.size.width, height: self.size.height))
            } else {
                self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
            }
            self.physicsBody?.usesPreciseCollisionDetection = true
            self.physicsBody?.allowsRotation = false;
            self.physicsBody?.density = 1.0
            self.physicsBody?.friction = 2.0
            self.physicsBody?.restitution = 0.0
            self.physicsBody!.categoryBitMask = PhysicsCategories.Character;
            self.physicsBody?.contactTestBitMask = PhysicsCategories.Collider | PhysicsCategories.House | PhysicsCategories.Swing | PhysicsCategories.Character
            self.physicsBody!.collisionBitMask = PhysicsCategories.House | PhysicsCategories.Object;
            self.physicsBody?.velocity = oldVel
        } else {
            if self.currentState.contains(SpriteOptions.swinging) {
                self.physicsBody = SKPhysicsBody(texture: self.texture!, alphaThreshold:0, size: CGSize(width: self.size.width, height: self.size.height))
            } else {
                self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
            }
            self.physicsBody?.usesPreciseCollisionDetection = true
            self.physicsBody?.allowsRotation = false;
            self.physicsBody?.density = 1.0
            self.physicsBody?.friction = 2.0
            self.physicsBody?.restitution = 0.0
            self.physicsBody!.categoryBitMask = PhysicsCategories.Character;
            self.physicsBody?.contactTestBitMask = PhysicsCategories.Collider | PhysicsCategories.House | PhysicsCategories.Swing | PhysicsCategories.Character
            self.physicsBody!.collisionBitMask = PhysicsCategories.House | PhysicsCategories.Object;
        }

    }
    
    init(doll: Doll, size: CGSize) {
        viewSize = size
        
        let texture = SKTexture(imageNamed: doll.name)
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
//        self.setScale(scaler)
        self.zPosition = 2;
        self.setPhysics()
        self.currentState.insert(SpriteOptions.stringToSprite(string: doll.startRoom)!);
        
        self.doll = doll
        print(doll.name, " size is ", self.size)
        
        let heightScale = self.size.height/736
        let widthScale = self.size.width/self.size.height
        let height = heightScale*(viewSize?.height)!
        let width = widthScale*height
        self.size = CGSize(width: width*1.75, height: height*1.75)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
