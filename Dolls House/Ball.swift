//
//  Ball.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/1/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class Ball: HMSprite {
    
    var viewSize :CGSize?
    
    init(size: CGSize, image: String) {
        let texture = SKTexture(imageNamed: image)
        
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
        
        viewSize = size
        
        if image == "basketball" {
            self.size = resize(sprite: self, scale: 0.14)
        } else {
            self.size = resize(sprite: self, scale: 0.22)
        }
        self.zPosition = 2
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.width/2)
        self.physicsBody?.usesPreciseCollisionDetection = true;
        self.physicsBody?.restitution = 0.9
        self.physicsBody?.allowsRotation = true;
        self.physicsBody?.mass = 0.5
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func throwBall(to location: CGPoint) {
        self.physicsBody?.velocity = CGVector(dx: 0.0, dy: 0.0)
        self.physicsBody?.isDynamic = true;
        let dx = location.x-self.position.x;
        let dy = location.y-self.position.y;
        let drag:CGFloat = 0.01
        var vel = CGVector(dx: dx/drag, dy: dy/drag)
        vel = velocityCheck(vel: vel);
        self.physicsBody!.velocity = vel;
    }
    
    // Ball needs to check itself before it wrecks itself and everything else in the scene
    func velocityCheck(vel: CGVector) -> CGVector {
        var velocity = vel;
        let MAX_BALL_VELOCITY = CGFloat(2500.0)
        if (velocity.dx > MAX_BALL_VELOCITY) {
            velocity.dx = MAX_BALL_VELOCITY
        } else if (velocity.dx < -MAX_BALL_VELOCITY) {
            velocity.dx = -MAX_BALL_VELOCITY
        }
        if (velocity.dy > MAX_BALL_VELOCITY) {
            velocity.dy = MAX_BALL_VELOCITY
        } else if (velocity.dy < -MAX_BALL_VELOCITY) {
            velocity.dy = -MAX_BALL_VELOCITY
        }
        return velocity;
    }

    override func update(dt: TimeInterval) {
        if let location = desiredLocation {
            let dt = CGFloat(dt)
            let dx = location.x-self.position.x
            let dy = location.y-self.position.y
            var vel = CGVector(dx: dx/dt, dy: dy/dt)
            vel = velocityCheck(vel: vel);
            self.physicsBody!.velocity = vel
        }
    }
    
    func getPhysics() -> SKPhysicsBody {
        return self.physicsBody!
    }
    
    func resize(sprite: HMSprite, scale: CGFloat) -> CGSize {
        let widthScale = sprite.size.width/sprite.size.height
        let height = scale*(viewSize?.height)!
        let width = widthScale*height
        let size =  CGSize(width: width, height: height)
        return size
    }

    
}
