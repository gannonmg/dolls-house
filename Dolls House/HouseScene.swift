//
//  HouseScene.swift
//  Dolls House
//
//  Created by Matt Gannon on 1/17/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import SpriteKit
import GameplayKit

struct PhysicsCategories {
    static let None : UInt32 = 0
    static let Character : UInt32 = 0b1 //1
    static let Object : UInt32 = 0b10   //2
    static let House : UInt32 = 0b100   //3
    static let Collider : UInt32 = 0b1000   //4
    static let Swing : UInt32 = 0b10000     //5
    static let Feeder : UInt32 = 0b100000   //6
}

class HouseScene: SKScene, SKPhysicsContactDelegate {

    //view size, height, and width for eazy access
    var viewSize = CGSize();
    var viewHeight = CGFloat();
    var viewWidth = CGFloat();
    
    var touched:Bool = false
//    var location = CGPoint.zero
    var startLocation = CGPoint.zero
    
    //Camera setup
    let cameraNode = SKCameraNode();
    var desiredPosition = CGPoint();
    
    var polaroid :Polaroid?;
    
    // initalize a super fun bouncy ball
    var ball :Ball?;
    var basketball :Ball?
    
    //initialize less fun laundry basket
    var basket :LaundryBasket?;
    
    // initialize the house
    let house = House();
    
    // array for holding interactable sprites
    var charArray :[Character] = [Character]()    //array for only dad, mom, kids, dog
    var spriteArray :[HMSprite] = [HMSprite]()  //array for all nodes/sprites incl. objects & family
    var tapArray :[HMTapable] = [HMTapable]()
    
    //intialize clock
    let clock = Clock()
    
    //initialize swing
    var tireSwing :TireSwing?
    
    //initialize collision objects array
    var collisionArray :[CollisionObject] = []
    
//    var currentSpriteHeld :HMSprite?;
    var currentSpritesHeld :Dictionary<UITouch, SKNode> = [:]
    
    var startClock = false
    var clockTime :TimeInterval?
    
    //Add feeder
    let feeder = SKSpriteNode(imageNamed: "feeder")
    
    //Rainbow mode
    let rainbowActivator = SKSpriteNode(imageNamed: "heart")
    
    //Background music
    var backgroundMusic: SKAudioNode?
    
    override func didMove(to view: SKView) {
        super.didMove(to: view);
        
        //Swap these to see dev things
        view.showsPhysics = false;
        view.showsFPS = false;
        view.showsNodeCount = false;
        
        viewSize = view.frame.size;
        viewHeight = viewSize.height;
        viewWidth = viewSize.width;
        
//        viewSize = CGSize(width: 300, height: 300);
//        viewHeight = viewSize.height;
//        viewWidth = viewSize.width;
        
        self.physicsWorld.contactDelegate = self;
        
        house.setViewSize(size: viewSize);
        house.drawHouse();
        house.drawBorders();
        self.addChild(house);
        
        for border in house.borderArray {
            border.physicsBody!.categoryBitMask = PhysicsCategories.House;
            border.physicsBody?.contactTestBitMask = PhysicsCategories.Collider | PhysicsCategories.Character
        }
        
        //Camera stuff
        let xRange = SKRange(lowerLimit: 0, upperLimit: 3*viewWidth)
        let yRange = SKRange(lowerLimit: 0, upperLimit: 3*viewHeight)
        let cameraRange = SKConstraint.positionX(xRange, y: yRange)
        cameraNode.constraints = [cameraRange]
        
        self.addChild(cameraNode);
        camera = cameraNode;
        cameraNode.position.y += viewHeight;
        desiredPosition = cameraNode.position;
        
        //Background music
        
        let backgroundMusic = SKAudioNode(fileNamed: "DollHouseJams.aifc")
        self.addChild(backgroundMusic)
        
        if let asset = NSDataAsset(name: "DollsHouseTapable") {
            let data = asset.data
//            print(data)
            do {
                let tapables = try JSONDecoder().decode([Tapable].self, from: data)
                
                for tapableObject in tapables {
                    let tapable = HMTapable(tapable: tapableObject, size: viewSize)
                    print("Aspect ratio is: ", viewHeight/viewWidth)
                    if (((viewHeight/viewWidth) < 0.751) && (viewHeight/viewWidth) > 0.74) {
                        let x = tapableObject.ipadX
                        let y = tapableObject.ipadY
                        tapable.position = CGPoint(x: CGFloat(x)*viewWidth, y: CGFloat(y)*viewHeight);
                    } else {
                        let x = tapableObject.x
                        let y = tapableObject.y
                        tapable.position = CGPoint(x: CGFloat(x)*viewWidth, y: CGFloat(y)*viewHeight);
                    }
                    
                    tapArray.append(tapable)
                    self.addChild(tapable)
                    
                    //place oven in front of fridge
                    tapable.zPosition = 1
                    if tapableObject.name == "oven" || tapableObject.name == "plant1" {
                        tapable.zPosition = 2
                    }

                }
                
            } catch {
                dump(error)
                assertionFailure("\(error.localizedDescription). You fucked up. Check your tapable JSON")
            }
        }
        
        if let asset = NSDataAsset(name: "DollsHouse") {
            let data = asset.data
            print(data)
            do {
                let dolls = try JSONDecoder().decode([Doll].self, from: data)
//                dump(dolls)
                
                //polaroid stuff to connect heads and sprites
                polaroid = Polaroid(dolls: dolls, size: viewSize)
                guard let polaroid = self.polaroid else { return }
                spriteArray.append(polaroid)
                if (((viewHeight/viewWidth) < 0.751) && (viewHeight/viewWidth) > 0.74) {
                    polaroid.position = CGPoint(x: -viewWidth*0.39, y: viewHeight*4/11)
                } else {
                    polaroid.position = CGPoint(x: -viewWidth*0.429, y: viewHeight*4/11)
                }
                cameraNode.addChild(polaroid)
                
                for dollObject in dolls {
//                    print("\n")
//                    print(dollObject)
                    let doll = Character(doll: dollObject, size: viewSize)
                    doll.position = CGPoint(x: CGFloat(dollObject.x)*viewWidth, y: CGFloat(dollObject.y)*viewHeight);
                    doll.name = dollObject.name
                    spriteArray.append(doll)
                    charArray.append(doll)
                    self.addChild(doll)
                    
                    let head = Head(imageNamed: dollObject.head, sprite: doll)
                    polaroid.headArray.append(head)
                }
                
                for head in polaroid.headArray {
                    head.position = polaroid.position;
                    head.setScale(0.0);
                    cameraNode.addChild(head)
                }

            } catch {
                dump(error)
                assertionFailure("\(error.localizedDescription). You fucked up. Check your main JSON")
            }
        }
        
        
        tireSwing = TireSwing(position: CGPoint(x: 3.25*viewWidth, y: 1.2*viewHeight), size: viewSize);
        tireSwing?.addToScene(scene: self)
        for piece in (tireSwing?.swingArray)! {
            piece.zPosition = 1
        }
        
        //add clock
        if (((viewHeight/viewWidth) < 0.751) && (viewHeight/viewWidth) > 0.74) {
            clock.position = CGPoint(x: viewWidth*0.39, y: viewHeight*4/11)
        } else {
            clock.position = CGPoint(x: viewWidth*0.429, y: viewHeight*4/11)
        }
        clock.zPosition = 50
        clock.size = resize(sprite: clock, scale: 0.1)
        cameraNode.addChild(clock)
        
        //add collison objects
        if let asset = NSDataAsset(name: "DollsHouseColliders") {
            let data = asset.data
//            print(data)
            do {
                print("did")
                let colliders = try JSONDecoder().decode([Collider].self, from: data)
                
                for collider in colliders {
                    let collisionObject = CollisionObject(myCollider: collider, size: viewSize)
                    collisionObject.name = collider.name
                    if (((viewHeight/viewWidth) < 0.751) && (viewHeight/viewWidth) > 0.74) {
                        collisionObject.position = CGPoint(x: CGFloat(collider.ipadX)*viewWidth, y: CGFloat(collider.ipadY)*viewHeight)
                    } else {
                        collisionObject.position = CGPoint(x: CGFloat(collider.x)*viewWidth, y: CGFloat(collider.y)*viewHeight)
                    }
                    collisionArray.append(collisionObject)
                }
                
            } catch {
                dump(error)
                assertionFailure("\(error.localizedDescription). You fucked up. Check your collider JSON")
            }
        }

        
        for object in collisionArray {
            object.physicsBody = SKPhysicsBody(rectangleOf: object.size)
            object.physicsBody?.isDynamic = false
            object.physicsBody?.restitution = 0.0
            object.physicsBody!.categoryBitMask = PhysicsCategories.Collider;
            object.physicsBody?.contactTestBitMask = PhysicsCategories.Collider | PhysicsCategories.Character
            self.addChild(object)
        }
        
        //Add tapable space to backyard to engage rainbow mode
        
        rainbowActivator.zPosition = 1
        if (((viewHeight/viewWidth) < 0.751) && (viewHeight/viewWidth) > 0.74) {
            rainbowActivator.setScale(1.5)
            rainbowActivator.position = CGPoint(x: 3.44*viewWidth, y: viewHeight)
        } else {
            rainbowActivator.setScale(0.75)
            rainbowActivator.position = CGPoint(x: 3.443*viewWidth, y: viewHeight)
        }
        self.addChild(rainbowActivator)
        
        //Add ball physics
        ball = Ball(size: viewSize, image: "ball")
        ball?.getPhysics().categoryBitMask = PhysicsCategories.Object;
        ball?.getPhysics().collisionBitMask = PhysicsCategories.House | PhysicsCategories.Object | PhysicsCategories.Character
        ball?.position = CGPoint(x: 0, y: viewHeight)
        ball?.zPosition = 1
        self.addChild(ball!);
        
        basketball = Ball(size: viewSize, image: "basketball")
        basketball?.getPhysics().categoryBitMask = PhysicsCategories.Object;
        basketball?.getPhysics().collisionBitMask = PhysicsCategories.House | PhysicsCategories.Object | PhysicsCategories.Character
        basketball?.position = CGPoint(x: 0, y: viewHeight)
        basketball?.zPosition = 1
        self.addChild(basketball!);

        
        //Add basket physics
        basket = LaundryBasket(size: viewSize)
        basket?.getPhysics().categoryBitMask = PhysicsCategories.Object;
        basket?.getPhysics().collisionBitMask = PhysicsCategories.House | PhysicsCategories.Object | PhysicsCategories.Character
        basket?.zPosition = 1
        self.addChild(basket!);
        
        //Add feeder and physics
        feeder.size = resize(sprite: feeder, scale: 0.25)
        feeder.physicsBody = SKPhysicsBody(rectangleOf: feeder.size)
        feeder.physicsBody?.categoryBitMask = PhysicsCategories.Feeder
        feeder.physicsBody?.collisionBitMask = PhysicsCategories.House
        feeder.physicsBody?.contactTestBitMask = PhysicsCategories.Character
        feeder.position = CGPoint(x: 2.2*viewWidth, y: viewHeight)
        feeder.physicsBody?.allowsRotation = false;
        feeder.name = "feeder"
//        feeder.setScale(0.5)
        feeder.zPosition = 1
        self.addChild(feeder);

        
        //Place characters
        setSprites()

        //Add moveable objects to spriteArray
        spriteArray.append(ball!)
        spriteArray.append(basketball!)
        spriteArray.append(clock)
        spriteArray.append(tireSwing!)
        spriteArray.append(basket!)
    }
    
    
    
    func setSprites() {
        let flash = SKShapeNode(rectOf: CGSize(width: viewWidth, height: viewHeight));
        flash.fillColor = SKColor.white
        let fadeIn = SKAction.fadeIn(withDuration: 0.4)
        let fadeOut = SKAction.fadeOut(withDuration: 0.7)
        let fadeSequence = SKAction.sequence([fadeIn, fadeOut])
        flash.zPosition = 100
        cameraNode.addChild(flash)
        flash.run(fadeSequence)

        
        ball?.position = CGPoint(x: viewWidth*4/3, y: 2*viewHeight)
        basketball?.position = CGPoint(x: 1.4*viewWidth, y: 0)
        basket?.position = CGPoint(x: 1.95*viewWidth, y: 0)
        
        if !(tireSwing?.emptySwing)! {
            if let sprite = tireSwing?.tire.childNode(withName: "tireChild") as? Character {
                sprite.removeFromParent()
                self.addChild(sprite)
                sprite.name = sprite.doll?.name
                sprite.physicsBody?.isDynamic = true
                sprite.physicsBody?.affectedByGravity = true
                sprite.zRotation = 0;
            }
        }
        
        for object in collisionArray {
            if !object.children.isEmpty {
                if let sprite = object.childNode(withName: "collisionChild") as? Character {
                    sprite.removeFromParent()
                    self.addChild(sprite)
                    sprite.name = sprite.doll?.name
                    sprite.physicsBody?.isDynamic = true
                    sprite.physicsBody?.affectedByGravity = true
                    sprite.zRotation = 0;
                    sprite.moveChar(to: nil)
                    object.empty = true
                }
            }
        }
        
        for tapper in tapArray {
            tapper.first = true
            tapper.turnOff()
        }
        
        for doll in charArray {
            var copy = doll.currentState
            copy.remove(SpriteOptions.rooms)
            copy.insert(SpriteOptions.stringToSprite(string: (doll.doll?.startRoom)!)!)
            doll.currentState = copy
            
            doll.moveChar(to: nil)
            doll.position = CGPoint(x: CGFloat((doll.doll?.x)!)*viewWidth, y: CGFloat((doll.doll?.y)!)*viewHeight);
            doll.stance(to: SpriteOptions.idle)
        }
    }
        
    //Code for drag and drop sprites
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let polaroid = self.polaroid else { return }
        touched = true;
        for touch in touches {
            startLocation = touch.location(in: self)
            let location = touch.location(in: self)
            let camLocation = touch.location(in: cameraNode)
            if (polaroid.headsShown) {
                for sprite in polaroid.headArray {
                    if(sprite.contains(camLocation)) {
                        relocate(sprite: sprite);
                        return;
                    }
                }
            }
            
            if rainbowActivator.contains(location) {
                for room in house.floor1Array {
                    if room.name == "backyard" {
                        if house.rainbow {
                            let texture = SKTexture(imageNamed: "backyardRainbow")
                            room.texture = texture
                            house.rainbow = false
                        } else {
                            let texture = SKTexture(imageNamed: "backyard1")
                            room.texture = texture
                            house.rainbow = true
                        }
                    }
                }
            }
            
            for object in collisionArray {
                if !object.empty {
                    if object.contains(location) {
                        if let sprite = object.childNode(withName: "collisionChild") as? Character {
                            currentSpritesHeld[touch] = sprite;
                            sprite.move(toParent: self)
                            sprite.stance(to: SpriteOptions.pickedUp)
                            sprite.zPosition = 41;
                            sprite.move(toParent: self)
                            spriteArray.remove(at: spriteArray.index(of: sprite)!)
                            spriteArray.insert(sprite, at: 0)
                            
                            sprite.physicsBody?.isDynamic = true
                            sprite.physicsBody?.affectedByGravity = true
                            sprite.position = object.position
                            sprite.moveChar(to: location)
                            currentSpritesHeld[touch] = sprite;
                            
                            return;
                        }
                    }
                }
            }

            if !feeder.children.isEmpty {
                let feederLocation = touch.location(in: feeder)
                for dogSprite in spriteArray {
                    if dogSprite.name == "dog" && dogSprite.contains(feederLocation) {
                        dogSprite.move(toParent: self)
                        currentSpritesHeld[touch] = dogSprite
                    }
                }
            }
            
            for sprite in spriteArray {
                if (sprite.contains(camLocation)) {
                    if (sprite == polaroid) {
                        if (polaroid.headsShown) {
                            polaroid.hideChars();
                        } else {
                            polaroid.showChars();
                        }
                        return;
                    } else if (sprite == clock) {
                        self.physicsWorld.gravity = CGVector(dx: 0, dy: 2.5)
                        startClock = true
                        currentSpritesHeld[touch] = clock
                        return;
                    }
                }
                
                if sprite == tireSwing {
                    if (sprite.contains(location)) {
                        let swingLocation = touch.location(in: tireSwing!.tire)
                        for child in (tireSwing?.tire.children)! {
                            if child.contains(swingLocation) {
                                if let newSprite = child as? Character {
                                    newSprite.removeFromParent()
                                    self.addChild(newSprite)
                                    newSprite.name = newSprite.doll?.name
                                    newSprite.physicsBody?.isDynamic = true
                                    newSprite.physicsBody?.affectedByGravity = true
                                    newSprite.zRotation = 0;
                                    newSprite.position = (tireSwing?.position)!
                                    newSprite.moveChar(to: location)
                                    currentSpritesHeld[touch] = newSprite;
                                    newSprite.stance(to: SpriteOptions.pickedUp)
                                    newSprite.zPosition = 41;
                                    spriteArray.remove(at: spriteArray.index(of: newSprite)!)
                                    spriteArray.insert(newSprite, at: 0)
                                    return;
                                }
                            } else {
                                currentSpritesHeld[touch] = tireSwing;
                            }
                        }
                    }
                }
                
                if(sprite.contains(location)) {
                    currentSpritesHeld[touch] = sprite;
                    sprite.stance(to: SpriteOptions.pickedUp)
                    sprite.zPosition = 41;
                    sprite.move(toParent: self)
                    let spriteLocation = CGPoint(x: location.x, y: location.y-sprite.size.height/2)
                    sprite.moveChar(to: spriteLocation)
                    spriteArray.remove(at: spriteArray.index(of: sprite)!)
                    spriteArray.insert(sprite, at: 0)
                    return;
                }
            }
            
            var hasCamera = false
            for (_, value) in currentSpritesHeld {
                if value == cameraNode { hasCamera = true }
            }
            if !hasCamera { currentSpritesHeld[touch] = cameraNode }
            
            for tapable in tapArray {
                if tapable.contains(location) {
                    tapable.switchState()
                    return
                }
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
//            print ("value is: ", currentSpritesHeld[touch] as Any)
            let location = touch.location(in: self)
            let startLocation = touch.previousLocation(in: self)
            if let sprite = currentSpritesHeld[touch] as? TireSwing {
                if sprite.restrain(touch: touch, scene: self, viewSize: viewSize) {
                    currentSpritesHeld[touch] = nil
                }
            } else if let sprite = currentSpritesHeld[touch] as? HMSprite {
                if sprite is Character {
                    let spriteLocation = CGPoint(x: location.x, y: location.y-sprite.size.height/2)
                    sprite.moveChar(to: spriteLocation)
                } else {
                    sprite.moveChar(to: location)
                }
            } else if currentSpritesHeld[touch] == cameraNode {
                let dx = (startLocation.x - location.x)
                let dy = (startLocation.y - location.y)
                let cameraPos = desiredPosition;
                let newCameraPos = CGPoint(x: cameraPos.x+dx, y: cameraPos.y + dy);
                desiredPosition = newCameraPos
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touched = false;
        for touch in touches {
            let location = touch.location(in: self)
//            let startLocation = touch.previousLocation(in: self)
            if let sprite = currentSpritesHeld[touch] as? HMSprite {
                if sprite == clock {
                    self.physicsWorld.gravity = CGVector(dx: 0, dy: -9.8)
                    startClock = false
                    clockTime = nil
                } else {
                    sprite.moveChar(to: nil)
                    sprite.stance(to: SpriteOptions.idle)
                }
            } else if currentSpritesHeld[touch] == cameraNode {
                let impulseX = CGFloat(4.0)
                let impulseY = CGFloat(impulseX * (viewHeight/viewWidth))
                let dx = impulseX * (startLocation.x - location.x)
                let dy = impulseY * (startLocation.y - location.y)
                desiredPosition = CGPoint(x: desiredPosition.x+dx, y: desiredPosition.y + dy);
                desiredPosition = closestRoom(current: desiredPosition);
            }
            if (tireSwing?.tire.children.isEmpty)! {
                tireSwing?.emptySwing = true;
            }
            for object in collisionArray {
                if object.children.isEmpty {
                    object.empty = true
                }
            }
            currentSpritesHeld[touch] = nil
        }
        
        //zPosition when idles
        for sprite in spriteArray {
            let z = 40 - spriteArray.index(of: sprite)!
            if z > 0 {
                sprite.zPosition = CGFloat(z)
            } else {
                sprite.zPosition = 1
            }
        }
        for piece in (tireSwing?.swingArray)! {
            piece.zPosition = 1
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touched = false;
        for touch in touches {
            let location = touch.location(in: self)
//            let startLocation = touch.previousLocation(in: self)
            if let sprite = currentSpritesHeld[touch] as? HMSprite {
                if sprite == clock {
                    self.physicsWorld.gravity = CGVector(dx: 0, dy: -9.8)
                    startClock = false
                    clockTime = nil
                } else {
                    sprite.moveChar(to: nil)
                    sprite.stance(to: SpriteOptions.idle)
                }
            } else if currentSpritesHeld[touch] == cameraNode {
                let impulseX = CGFloat(4.0)
                let impulseY = CGFloat(impulseX * (viewHeight/viewWidth))
                let dx = impulseX * (startLocation.x - location.x)
                let dy = impulseY * (startLocation.y - location.y)
                desiredPosition = CGPoint(x: desiredPosition.x+dx, y: desiredPosition.y + dy);
                desiredPosition = closestRoom(current: desiredPosition);
            }
            if (tireSwing?.tire.children.isEmpty)! {
                tireSwing?.emptySwing = true;
            }
            for object in collisionArray {
                if object.children.isEmpty {
                    object.empty = true
                }
            }
            currentSpritesHeld[touch] = nil
        }
        
        //zPosition when idles
        for sprite in spriteArray {
            let z = 40 - spriteArray.index(of: sprite)!
            if z > 0 {
                sprite.zPosition = CGFloat(z)
            } else {
                sprite.zPosition = 1
            }
        }
        for piece in (tireSwing?.swingArray)! {
            piece.zPosition = 1
        }
    }
    
    var previousTime = TimeInterval();
    var isFirstFrame = true;
    
    override func update(_ currentTime: TimeInterval) {
        if isFirstFrame {
            isFirstFrame = false
        } else {
            cameraNode.position = lerp(a: cameraNode.position, b: desiredPosition, t: 0.2);
            for sprite in spriteArray {
                let dt = currentTime - previousTime
                sprite.update(dt: dt)
            }
            previousTime = currentTime
            
            if startClock {
                clockTime = currentTime
                startClock = false
            }
            if clockTime != nil {
                let timeDiff = currentTime - clockTime!
                if timeDiff > 0.65 {
                    setSprites()
                    clockTime = nil
//                    currentSpriteHeld = nil
                    self.physicsWorld.gravity = CGVector(dx: 0, dy: -9.8)

                }
            }
            
            if tireSwing?.redundantRestrain() == true {
                for (touch, value) in currentSpritesHeld {
                    if tireSwing == value {
                        currentSpritesHeld[touch] = nil
                    }
                }
            }
            
            tireSwing?.zPosition = 1
            for piece in (tireSwing?.swingArray)! {
                piece.zPosition = 1
            }

            for child in (tireSwing?.tire.children)! {
                if let sprite = child as? Character {
                    sprite.stance(to: SpriteOptions.swinging)
                    sprite.zPosition = (tireSwing?.tire.zPosition)! + 1
                    sprite.physicsBody?.affectedByGravity = false
                    sprite.physicsBody?.isDynamic = false
                    sprite.moveChar(to: (CGPoint(x: (tireSwing?.tire.position.x)!, y: (tireSwing?.tire.position.y)!-(tireSwing?.tire.size.height)!/2)))
                    sprite.position = CGPoint(x: (tireSwing?.tire.position.x)!, y: (tireSwing?.tire.position.y)!-(tireSwing?.tire.size.height)!/2)
                    sprite.zRotation = 0;
                }
            }
            
            for object in collisionArray {
                for child in object.children {
                    if let sprite = child as? Character {
                        if object.name == "toilet" {
                            let position = CGPoint(x: sprite.size.width*CGFloat((sprite.doll?.toiletX)!), y: sprite.size.height*CGFloat((sprite.doll?.toiletY)!))
                            sprite.moveChar(to: position)
                        } else if (sprite.doll?.name == "dad") && (sprite.currentState.contains(SpriteOptions.laying)){
                            sprite.moveChar(to: CGPoint(x: 0, y: -viewHeight/20))
                        } else {
                            sprite.moveChar(to: CGPoint.zero)
                        }
                        sprite.zPosition = 3
                        sprite.stance(to: object.givesState())
                    }
                }
            }
        }
    }
    
    
    //Contact managers
    func didBegin(_ contact: SKPhysicsContact) {
        if let object = contact.bodyA.node as? CollisionObject? ?? contact.bodyB.node as? CollisionObject? {
            if let char = contact.bodyA.node as? Character ?? contact.bodyB.node as? Character {
                if !char.currentState.contains(SpriteOptions.pickedUp) {
                    object?.addCollisionChild(sprite: char)
                }
            }
        } else if let char = contact.bodyA.node as? Character ?? contact.bodyB.node as? Character {
            if let object = contact.bodyB.node as? SKSpriteNode? ?? contact.bodyA.node as? SKSpriteNode? {
                if object?.name == "swing" {
                    if (tireSwing?.swingArray.contains(object!))! {
                        if !char.currentState.contains(SpriteOptions.pickedUp) {
                            tireSwing?.addTireChild(sprite: char)
                        }
                    }
                }
                if object?.name == "feeder" {
                    if !char.currentState.contains(SpriteOptions.pickedUp) {
                        if char.doll?.states["eating"] != nil {
//                            char.moveChar(to: CGPoint(x: feeder.position.x-feeder.size.width/2, y: feeder.position.y))
                            char.stance(to: SpriteOptions.eating)
                            char.move(toParent: feeder)
                            char.zPosition = feeder.zPosition+1
                            char.moveChar(to: CGPoint(x: -feeder.size.width/2, y: 0))
                            let soundAction = SKAction.playSoundFileNamed("dog_eating.aifc", waitForCompletion: false)
                            char.run(soundAction)
                        }
                    }
                }
            }
        }
        if let char1 = contact.bodyA.node as? Character ?? contact.bodyB.node as? Character {
            if let char2 = contact.bodyB.node as? Character? ?? contact.bodyA.node as? Character? {
                if char1.doll?.name != char2?.doll?.name {
                    if let stateGiven = char1.doll?.givesState {
                        let canInteract = char2?.doll?.states[stateGiven] != nil
                        if canInteract && (!(char2?.currentState.contains(SpriteOptions.pickedUp))!) {
                            char2?.stance(to: SpriteOptions.stringToSprite(string: stateGiven)!)
                        }
                    }
                }
            }
        }
    }
    
    
    func didEnd(_ contact: SKPhysicsContact) {
        if let char1 = contact.bodyA.node as? Character ?? contact.bodyB.node as? Character {
            if let char2 = contact.bodyB.node as? Character? ?? contact.bodyA.node as? Character? {
                if char1.doll?.name != char2?.doll?.name {
                    if char1.doll?.givesState != nil {
                        for (_, value) in currentSpritesHeld {
                            if let currentSpriteHeld = value as? Character {
                                if char2  == currentSpriteHeld {
                                    char2?.stance(to: SpriteOptions.pickedUp)
                                } else {
                                    char2?.stance(to: SpriteOptions.idle)
                                }
                            }
                        }
                    }
                }
            }
        } else if let char = contact.bodyA.node as? Character ?? contact.bodyB.node as? Character {
            if let object = contact.bodyB.node as? SKSpriteNode? ?? contact.bodyA.node as? SKSpriteNode? {
                if object?.name == "feeder" {
                    for (_, value) in currentSpritesHeld {
                        if let currentSpriteHeld = value as? Character {
                            if char == currentSpriteHeld {
                                char.stance(to: SpriteOptions.pickedUp)
                            } else {
                                char.stance(to: SpriteOptions.idle)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func closestRoom(current: CGPoint) -> CGPoint {
        var closest = current
        var min = CGFloat(1000000.0);
        for center in house.centerArray {
            let dist = hypotf(Float(current.x - center.x), Float(current.y - center.y));
            if (CGFloat(dist) < min) {
                for floor in house.allFloorsArray {
                    for room in floor {
                        if (room.position.equalTo(center)) && (room.name != nil) {
                            min = CGFloat(dist);
                            closest = center;
                        }
                    }
                }
            }
        }
        return closest
    }
    
    func relocate(sprite: Head) {
        if let room = getCurrentRoom() {
            if let mySprite = sprite.mySprite {
                mySprite.removeFromParent()
                self.addChild(mySprite)
                mySprite.physicsBody?.velocity = CGVector.zero
                mySprite.moveChar(to: nil)
                mySprite.stance(to: SpriteOptions.idle);
                mySprite.set(in: room)
            }
        }
    }
    
    func getCurrentRoom() -> SKSpriteNode? {
        let current = cameraNode.position
        var min = CGFloat(1000000.0);
        var currentRoom :SKSpriteNode?
        for center in house.centerArray {
            let dist = hypotf(Float(current.x - center.x), Float(current.y - center.y));
            if (CGFloat(dist) < min) {
                min = CGFloat(dist);
                for floor in house.allFloorsArray {
                    for room in floor {
                        if (room.position.equalTo(center)) && (room.name != nil) {
                            currentRoom = room;
                        }
                    }
                }
            }
        }
        return currentRoom;
    }
    
    func getWidth() -> CGFloat {
        return viewWidth;
    }
    
    func getHeight() -> CGFloat {
        return viewHeight;
    }
    
    func resize(sprite: SKSpriteNode, scale: CGFloat) -> CGSize {
        let widthScale = sprite.size.width/sprite.size.height
        let height = scale*viewSize.height
        let width = widthScale*height
        let size =  CGSize(width: width, height: height)
        return size
    }

}

public func lerp(a: CGPoint, b: CGPoint, t: CGFloat) -> CGPoint {
    let x = a.x + (b.x-a.x) * t;
    let y = a.y + (b.y-a.y) * t;
    return CGPoint(x: x, y: y);
}

public func CGDistance(from a: CGPoint, to b: CGPoint) -> CGFloat {
    let xDist = (b.x - a.x);
    let yDist = (b.y - a.y);
    let distance = sqrt((xDist * xDist) + (yDist * yDist));
    return distance;
}

