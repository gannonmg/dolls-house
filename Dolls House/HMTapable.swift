//
//  Tapable.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/20/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class HMTapable: SKSpriteNode {
    
    var first = true
    
    var tapable :Tapable?
    var viewSize :CGSize?
    
    var paintingCounter = 1
    
    var currentState :TapOptions = [.off] {
        didSet {
            let switchName = TapOptions.stateToString(currentState)
            onStateChanged(switchName)
        }
    }
    
    func onStateChanged(_ switchState: String?) {
        guard let tapable = tapable else { return }
        guard let switchState = switchState else { return }
        
        self.removeAllActions()
        
        if let state = tapable.states[switchState] {
            
            if tapable.name == "painting1" || tapable.name == "painting6" {
                if let frames = state.animation {
                    var textureArray :[SKTexture] = [SKTexture]()
                    for frame in frames {
                        let texture = SKTexture(imageNamed: frame)
                        textureArray.append(texture)
                    }
                    
                    let next = (paintingCounter%textureArray.count)
                    let texture = textureArray[next]
                    self.texture = texture
                }
                paintingCounter+=1
                return
            }
            
            if let frames = state.animation {
                var textureArray :[SKTexture] = [SKTexture]()
                for frame in frames {
                    let texture = SKTexture(imageNamed: frame)
                    textureArray.append(texture)
                }
                let animate = SKAction.animate(with: textureArray, timePerFrame: 0.7)
                self.run(SKAction.repeatForever(animate))
            } else {
                let texture = SKTexture(imageNamed: state.texture)
                self.texture = texture
            }
            
            if !first {
                if let sound = state.sound {
                    let soundAction = SKAction.playSoundFileNamed(sound, waitForCompletion: true)
                    self.run(soundAction)
                }
            } else {
                first = false
            }
            
            self.size = (self.texture?.size())!
            if let viewHeight = (viewSize?.height) {
                let viewWidth = (viewSize?.width)!
                if (((viewHeight/viewWidth) < 0.751) && (viewHeight/viewWidth) > 0.74) {
                    self.size = resize(sprite: self, scale: CGFloat(tapable.ipadScale))
                } else {
                    self.size = resize(sprite: self, scale: CGFloat(tapable.scale))
                }
            }
        }
    }
    
    
    init(tapable: Tapable, size: CGSize) {
        let texture = SKTexture(imageNamed: tapable.name)
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
        
        
        viewSize = size
        let viewHeight = size.height
        let viewWidth = size.width
        self.zPosition = 1
        
//        print("Aspect ratio in tapable is: ", viewHeight/viewWidth)
        if (((viewHeight/viewWidth) < 0.751) && (viewHeight/viewWidth) > 0.74) {
            self.size = resize(sprite: self, scale: CGFloat(tapable.ipadScale))
        } else {
            self.size = resize(sprite: self, scale: CGFloat(tapable.scale))
        }

        
        self.tapable = tapable
    }
    
    func resize(sprite: HMTapable, scale: CGFloat) -> CGSize {
        let widthScale = sprite.size.width/sprite.size.height
        let height = scale*(viewSize?.height)!
        let width = widthScale*height
        let size =  CGSize(width: width, height: height)
        return size
    }
    
    func switchState() {
        guard let tapable = tapable else { return }
        var copy = currentState

//        if tapable.name == "painting" {
//            copy.remove(.allStates)
//            copy.insert(.on)
//            currentState = copy
//            return
//        }
        
        if tapable.states["filled"] != nil {
            if copy.contains(.on) {
                copy.remove(.allStates)
                copy.insert(.filled)
            } else {
                if copy.contains(.off) {
                    copy.remove(.allStates)
                    copy.insert(.on)
                } else {
                    copy.remove(.allStates)
                    copy.insert(.off)
                }
            }
        } else {
            if copy.contains(.off) {
                copy.remove(.allStates)
                copy.insert(.on)
            } else {
                copy.remove(.allStates)
                copy.insert(.off)
            }
        }
        
        currentState = copy
    }
    
    func turnOff() {
        var copy = currentState
        copy.remove(.on)
        copy.insert(.off)
        currentState = copy
    }
    
    init(texture: SKTexture!) {
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
    }
    
    override init(texture: SKTexture!, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
