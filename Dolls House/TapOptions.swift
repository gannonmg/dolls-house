//
//  TapOptions.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/20/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation

struct TapOptions: OptionSet {
    let rawValue: Int
    
    static let on = TapOptions(rawValue: 1 << 0)
    static let off = TapOptions(rawValue: 1 << 1)
    static let filled = TapOptions(rawValue: 1 << 2)
    
    static let allStates: TapOptions = [.on, .off, .filled]
    
    static func stringToState(_ string: String) -> TapOptions? {
        if string == "off" { return TapOptions.off }
        if string == "on" { return TapOptions.on }
        if string == "filled" { return TapOptions.filled }
        return nil
    }
    
    static func stateToString(_ state: TapOptions) -> String? {
        if state == TapOptions.off { return "off" }
        if state == TapOptions.on { return "on" }
        if state == TapOptions.filled { return "filled" }
        return nil
    }
}
