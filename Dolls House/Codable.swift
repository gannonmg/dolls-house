//
//  Codable.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/14/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

struct Doll : Codable {
    struct State : Codable {
        var texture:String
        var sound:String?
        var locations:Dictionary<String, String>?
        var animation:String?
    }
    
    struct Animation : Codable {
        var frames:[String]
        var speed:Int
        var startDirection:Int
        var repeats:Bool
    }
    
    var name:String
    var head:String
    var givesState:String?
    var x:Float
    var y:Float
    var toiletX:Float
    var toiletY:Float
    var startRoom:String
    var states: Dictionary<String, Doll.State>
    var animations: Dictionary<String, Doll.Animation>?

}

struct Tapable : Codable {
    
    struct State : Codable {
        var texture:String
        var sound:String?
        var animation:[String]?
    }

    var name:String
    var x:Float
    var y:Float
    var scale:Float
    var ipadX:Float
    var ipadY:Float
    var ipadScale:Float
    var states: Dictionary<String, Tapable.State>
}

struct Collider : Codable {
    var name:String
    var x:Float
    var y:Float
    var ipadX:Float
    var ipadY:Float
    var width:Float
    var height:Float
    var givesState:String
    var sound:String?
}
