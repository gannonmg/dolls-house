//
//  HMSprite.swift
//  Dolls House
//
//  Created by Matt Gannon on 2/1/18.
//  Copyright © 2018 happyMedium. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class HMSprite: SKSpriteNode {
    
    var desiredLocation :CGPoint?;
    var availableStates :Dictionary<String, String>?;
    
    var currentState :SpriteOptions = [.idle] {
        didSet {
            //Do state changes
        }
    }
    var givesState :SpriteOptions?
    
    init() {
        super.init(texture: nil, color: UIColor.clear, size: CGSize.zero)
    }
    
    init(texture: SKTexture!) {
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
    }
    
    override init(texture: SKTexture!, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func update(dt: TimeInterval) {
        if let location = desiredLocation {
            let dt = CGFloat(dt * 4)
            let dx = location.x-self.position.x
            let dy = location.y-self.position.y
            let vel = CGVector(dx: dx/dt, dy: dy/dt)
            self.physicsBody!.velocity = vel
        }
    }
        
    func set(in room: SKSpriteNode) {
        
        if let parent = self.parent as? CollisionObject {
            parent.empty = true;
        }
        
        var copy = currentState
        copy.remove(SpriteOptions.rooms)
        if let roomState = SpriteOptions.stringToSprite(string: room.name!) {
            copy.insert(roomState)
            currentState = copy;
        }
        self.position = room.position;
    }

    func stance(to stance: SpriteOptions) {
        var copy = currentState
        if !currentState.contains(stance) {
            copy.remove(SpriteOptions.stances)
            copy.insert(stance)
            currentState = copy
        }
    }
    
    func moveChar(to location: CGPoint?) {
        desiredLocation = location
    }
    
    func spriteRest() {
        self.physicsBody?.isDynamic = true;
    }
    
    
}



